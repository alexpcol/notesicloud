//
//  ViewController.swift
//  My Notes
//
//  Created by Mario on 7/27/18.
//  Copyright © 2018 Mario. All rights reserved.
//

import UIKit
import CloudKit
class ViewController: UIViewController {

    // MARK: - Variables
    @IBOutlet weak var tableView: UITableView!
    var notes = [Note] ()
    // MARK: - LifeCycleMethods
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        CloudKitService.shared.subscribeWithUI()
        UNService.shared.authorize()
        getNotes()
        
        NotificationCenter.default.addObserver(self, selector: #selector(handleFetch(_:)), name: NSNotification.Name("notidicationCenter.fetchedRecord"), object: nil)
    }
    // MARK: - General Methods
    
    @objc func handleFetch(_ sender: Notification)
    {
        UNService.shared.unCenter.removeAllDeliveredNotifications()
        UIApplication.shared.applicationIconBadgeNumber = 0
        guard let record = sender.object as? CKRecord,
            let note = Note(record: record) else {return}
        insert(note: note)
    }
    func getNotes(){
        NotesService.getNotes { (notes) in
            self.notes = notes
            self.tableView.reloadData()
        }
    }
    // MARK: - ActionMethods
    @IBAction func onComposeTapped(_ sender: Any)
    {
        AlertService.composeNote(in: self) { (note) in
            CloudKitService.shared.save(record: note.noteRecord())
            self.insert(note: note)
        }
    }
    func insert(note: Note)
    {
        notes.insert(note, at: 0)
        let indexPath = IndexPath(row: 0, section: 0)
        tableView.insertRows(at: [indexPath], with: .automatic)
        
    }
}
extension ViewController: UITableViewDataSource
{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return notes.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        //let cell = UITableViewCell()
        let note = notes[indexPath.row]
        cell.textLabel?.text = note.title + " 1"
        return cell
    }
}

extension ViewController: UITableViewDelegate
{
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        let deleteRow = UITableViewRowAction(style: .destructive, title: "Delete") { (rowAction, indexPath) in
            //self.delete(indexPath: indexPath, tableView: tableView)
        }
        return[deleteRow]
    }
}

