//
//  Note.swift
//  My Notes
//
//  Created by Mario on 7/27/18.
//  Copyright © 2018 Mario. All rights reserved.
//

import Foundation
import CloudKit

struct Note {
    let title: String
    static let recordType = "Note"
    
    init(title: String) {
        self.title = title
    }
    
    init?(record : CKRecord) {
        guard let title = record.value(forKey: "title") as? String else { return nil}
        self.title = title
    }
    
    func noteRecord () -> CKRecord
    {
        let record = CKRecord(recordType: Note.recordType)
        record.setValue(title, forKey: "title")
        return record
    }
}
