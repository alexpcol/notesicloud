//
//  AlertService.swift
//  My Notes
//
//  Created by Mario on 7/27/18.
//  Copyright © 2018 Mario. All rights reserved.
//
import UIKit


class AlertService: NSObject {
    private override init() {}
    
    static func composeNote(in vc: UIViewController, completion: @escaping (Note) -> Void)
    {
        let alert = UIAlertController(title: "New note", message: "What's on your mind?", preferredStyle: .alert)
        alert.addTextField { (titleTF) in
            titleTF.placeholder = "Enter new note"
        }
        let post = UIAlertAction(title: "Post", style: .default) { (_) in
            guard let title = alert.textFields?.first?.text else{return}
            let note = Note(title: title)
            completion(note)
        }
        alert.addAction(post)
        
        vc.present(alert, animated: true)
    }
    
}
