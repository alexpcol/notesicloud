//
//  CloudKitService.swift
//  My Notes
//
//  Created by chila on 10/15/18.
//  Copyright © 2018 Mario. All rights reserved.
//

import Foundation
import CloudKit

class CloudKitService{
    
    private init(){}
    static let shared = CloudKitService()
    
    let privateDatabase = CKContainer.default().privateCloudDatabase
    func save (record: CKRecord)
    {
        privateDatabase.save(record) { (record, error) in
            print(error ?? "No ck save error")
            print(record ?? "No ck record saved")
        }
    }
    func query(recordType: String, completion: @escaping ([CKRecord])->Void)
    {
        let query = CKQuery(recordType: recordType, predicate: NSPredicate(value: true))
        privateDatabase.perform(query, inZoneWith: nil) { (records, error) in
            print(error ?? "No CK query error")
            guard let records = records else {return}
            DispatchQueue.main.async {
                completion(records)
            }
        }
    }
    
    func subscribe()
    {
        let subscription = CKQuerySubscription(recordType: Note.recordType, predicate: NSPredicate(value: true), options: .firesOnRecordCreation)
        
        let notificationInfo = CKSubscription.NotificationInfo()
        notificationInfo.shouldSendContentAvailable = true
        subscription.notificationInfo = notificationInfo
        
        privateDatabase.save(subscription) { (sub, error) in
            print(error ?? "No ck sub error")
            print(sub ?? "Unable to subscribe")
        }
    }
    func subscribeWithUI()
    {
        let subscription = CKQuerySubscription(recordType: Note.recordType, predicate: NSPredicate(value: true), options: .firesOnRecordCreation)
        
        let notificationInfo = CKSubscription.NotificationInfo()
        if #available(iOS 11.0, *) {
            notificationInfo.title = "This is cool"
        } else {
            // Fallback on earlier versions
        }
        if #available(iOS 11.0, *) {
            notificationInfo.subtitle = "A whole nre iCloud"
        } else {
            // Fallback on earlier versions
        }
        notificationInfo.alertBody = "This so powerfull tho"
        notificationInfo.shouldBadge = true
        notificationInfo.soundName = "default"
        
        subscription.notificationInfo = notificationInfo

        
        privateDatabase.save(subscription) { (sub, error) in
            print(error ?? "No ck sub error")
            print(sub ?? "Unable to subscribe")
        }
    }
    func fetchRecord(with recordId: CKRecord.ID)
    {
        privateDatabase.fetch(withRecordID: recordId) { (record, error) in
            print(error ?? "No CK Fetch error")
            guard let record = record else {return}
            DispatchQueue.main.async {
                NotificationCenter.default.post(name: NSNotification.Name("notidicationCenter.fetchedRecord"), object: record)
            }
        }
    }
    func handleNotification(with userInfo: [AnyHashable: Any])
    {
        let notification = CKNotification(fromRemoteNotificationDictionary: userInfo)
        switch notification.notificationType {
        case .query:
            guard let queryNotification = notification as? CKQueryNotification, let recordId = queryNotification.recordID else {return}
            fetchRecord(with: recordId)
            break
        default: return
        }
    }
}
