//
//  NotesService.swift
//  My Notes
//
//  Created by chila on 10/15/18.
//  Copyright © 2018 Mario. All rights reserved.
//

import Foundation


class NotesService {
    private init (){}
    
    static func getNotes(completion : @escaping ([Note])->Void){
        CloudKitService.shared.query(recordType: Note.recordType) { (records) in
            var notes = [Note]()
            for record in records{
                if let note = Note(record: record) { notes.append(note)}
            }
            completion(notes)
        }
    }
}
