//
//  UNService.swift
//  My Notes
//
//  Created by chila on 10/15/18.
//  Copyright © 2018 Mario. All rights reserved.
//

import Foundation
import UserNotifications

class UNService: NSObject {
    private override init() {}
    static let shared = UNService()
    
    let unCenter = UNUserNotificationCenter.current()
    func authorize(){
        let options : UNAuthorizationOptions = [.alert, .badge, .sound]
        unCenter.requestAuthorization(options: options) { (granted, error) in
            print(error ?? "No un auth error")
            guard  granted else {return}
            self.configure()
        }
    }
    func configure ()
    {
        unCenter.delegate = self
    }
}

extension UNService: UNUserNotificationCenterDelegate
{
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        print("Un did receive")
        CloudKitService.shared.handleNotification(with: response.notification.request.content.userInfo)
        completionHandler()
    }
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        print("Un Will present")
        CloudKitService.shared.handleNotification(with: notification.request.content.userInfo)
        let options: UNNotificationPresentationOptions = [.alert, .sound]
        completionHandler(options)
    }
}
